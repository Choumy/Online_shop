package com.example.onlineshop.model.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private UUID id;
    private String brand;
    private String description;
    private BigDecimal price;




}
