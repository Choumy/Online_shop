package com.example.onlineshop.model.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


import java.math.BigDecimal;
import java.util.UUID;
@Data
@Table("tb_products")
public class Product {

    @Id
    @Column("prod_id")
    private UUID id;
    @Column("prod_brand")
    private String brand;
    @Column("description")
    private String description;
    @Column("price")
    private BigDecimal price;

    public Product(UUID id, String brand, String description, BigDecimal price) {
        this.id = id;
        this.brand = brand;
        this.description = description;
        this.price = price;
    }

}
