package com.example.onlineshop.model.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@Table("tb_person")
public class Person {

    @Id
    @Column("person_id")
    private UUID pid;
    @Column("username")
    private String username;
    @Column("email")
    private String email;
    @Column("password")
    private String password;
    @Column("role")
    private String role;

    public Person(UUID pid, String username, String email, String password, String role) {
        this.pid = pid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
    }
}
