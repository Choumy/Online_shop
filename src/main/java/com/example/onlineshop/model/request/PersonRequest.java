package com.example.onlineshop.model.request;

import com.example.onlineshop.model.entity.Person;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonRequest {

    private UUID id;
    private String username;
    private String email;
    private String password;
    private String role;

    public Person toEntity(){
        return new Person(
                null,
                this.username,
                this.email,
                this.password,
                this.role
        );
    }
}
