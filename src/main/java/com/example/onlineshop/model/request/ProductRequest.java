package com.example.onlineshop.model.request;

import com.example.onlineshop.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    private UUID id;
    private String brand;
    private String description;
    private BigDecimal price;

    public Product toEntity(){
        return new Product(
                null,
                this.brand,
                this.description,
                this.price
        );
    }
}
