package com.example.onlineshop.service.person;

import com.example.onlineshop.model.entity.Person;
import com.example.onlineshop.model.request.PersonRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class PersonServiceImp implements PersonService{

    private PersonRepository personRepository;

    @Override
    public Mono<Person> register(PersonRequest personRequest) {
        Person person = personRequest.toEntity();
        var res = personRepository.save(person);
        return res;
    }

    @Override
    public Flux<Person> getAllPerson() {
        return Flux.from(personRepository.findAll());
    }
}
