package com.example.onlineshop.service.person;

import com.example.onlineshop.model.entity.Person;
import com.example.onlineshop.model.request.PersonRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PersonService {

    Mono<Person> register(PersonRequest personRequest);

    Flux<Person> getAllPerson();
}
