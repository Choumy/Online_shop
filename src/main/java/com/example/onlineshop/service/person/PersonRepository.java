package com.example.onlineshop.service.person;

import com.example.onlineshop.model.entity.Person;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PersonRepository extends ReactiveCrudRepository<Person, UUID> {
}
