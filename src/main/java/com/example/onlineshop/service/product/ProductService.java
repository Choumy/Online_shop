package com.example.onlineshop.service.product;


import com.example.onlineshop.model.entity.Product;
import com.example.onlineshop.model.request.ProductRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface ProductService {

    Mono<Product> registerProduct(ProductRequest productRequest);

    Flux<Product> getAllProduct();

}
