package com.example.onlineshop.service.product;


import com.example.onlineshop.model.entity.Product;
import com.example.onlineshop.model.request.ProductRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class ProductServiceImp implements ProductService{

    private ProductRepository productRepository;


    @Override
    public Mono<Product> registerProduct(ProductRequest productRequest) {
        Product product = productRequest.toEntity();

        var res = productRepository.save(product);

        return res;
    }

    @Override
    public Flux<Product> getAllProduct() {


        return Flux.from(productRepository.findAll());
    }
}
