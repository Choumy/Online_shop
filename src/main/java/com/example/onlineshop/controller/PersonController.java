package com.example.onlineshop.controller;

import com.example.onlineshop.model.request.PersonRequest;
import com.example.onlineshop.service.person.PersonService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/person")
public class PersonController {

    private PersonService personService;
    @PostMapping("/register")
    public void register(@RequestBody PersonRequest personRequest){
        personService.register(personRequest);
    }
}
