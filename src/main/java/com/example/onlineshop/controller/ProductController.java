package com.example.onlineshop.controller;

import com.example.onlineshop.model.entity.Product;
import com.example.onlineshop.model.request.ProductRequest;
import com.example.onlineshop.service.product.ProductService;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/product")
@CrossOrigin("*")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public Flux<Product> getAllProduct(){

        return productService.getAllProduct();
    }

    @PostMapping("/register")
    public Mono<Product> registerProduct(@RequestBody ProductRequest productRequest){

        System.out.println("Product data: "+ productRequest);
        return productService.registerProduct(productRequest);
    }
}
