create table if not exists tb_products
(
    prod_id uuid default uuid_generate_v4() primary key,
    brand varchar(15) not null,
    description varchar(255),
    price decimal(12,2)
);
create table if not exists tb_persons(
    person_id uuid default uuid_generate_v4() primary key,
    username varchar(15) not null,
    email varchar(30) not null

);

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";